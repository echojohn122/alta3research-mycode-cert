#!/usr/bin/python3
""" Request and download an export of all gitlab repositories | Khiem Nguyen """

import getpass
import os
import time
import requests

def check_connectivity(host):
    """ Use Ping to check network connectivity """
    response = os.system("ping -qc 1 " + host + " 2>&1 > /dev/null")

    # check response
    return bool(response == 0)

def get_list_of_projects(host,headers):
    """ Return a list of projects owned by the user at the specified host"""
    projects_url = f"https://{host}/api/v4/projects/?simple=true&owned=true&per_page=100"

    # send the request
    projects_response = requests.get(projects_url, headers=headers)

    # strip the JSON off and return the object
    return projects_response.json()

def request_export(host,headers):
    """ Go through the list of projects owned by the user and request an export"""

    for project in get_list_of_projects(host, headers):
        print(f"Requesting export for project: {project['name']} ({project['id']})")
        requests.post(f"https://{host}/api/v4/projects/{project['id']}/export",headers=headers)

def download_export(host,headers):
    """ Go through the list of projects owned by the user and download export if it is complete"""

    count = 0
    for project in get_list_of_projects(host, headers):
        count += 1
        # send a request for export status
        export_status = requests.get(f"https://{host}/api/v4/projects/{project['id']}/export",\
                                    headers=headers).json()

        # Wait until the export is ready
        while export_status['export_status'] != 'finished':
            # If the export didn't start for some reason, send another request
            if export_status['export_status'] == 'none':
                requests.post(f"https://{host}/api/v4/projects/{project['id']}/export",\
                            headers=headers)
            time.sleep(10)
            export_status = requests.get(f"https://{host}/api/v4/projects/{project['id']}/export",\
                                        headers=headers).json()

        # Export should be ready for download
        if export_status['export_status'] == 'finished':
            # If we haven't download the project, then download it
            if not os.path.exists(f"{project['name']}-{project['id']}.tar.gz"):
                print(f"Downloading {project['name']}-{project['id']}.tar.gz")
                export_response = requests.get(f"https://{host}/api/v4/projects/{project['id']}/export/download",\
                                            headers=headers)

                # write to file
                with open(f"{project['name']}-{project['id']}.tar.gz", "wb") as project_file:
                    project_file.write(export_response.content)
            else:
                print(f"Skipping {project['name']}-{project['id']} download")
        else:
            # we shouldn't be here but just in case
            print(f"ERROR: project: {project['name']} ({project['id']}) \
                    status: {export_status['export_status']}")

    print(f"Projects count: {count}")

def main():
    """ run code """
    # Request the user enter the gitlab instance URL
    host = input("\nPlease enter the gitlab URL: (gitlab.com) ")
    if not host:
        host = "gitlab.com"

    # Request the user enter the private token
    private_token = getpass.getpass()
    headers = {
        'Private-Token': private_token
    }

    # Check connectivity.
    # If successful then go and request and retrieve export
    print(f"Checking connectivity for {host}")
    if check_connectivity(host):
        print("Connectivity confirmed!")
        request_export(host,headers)
        download_export(host, headers)
    else:
        print(f"Could not establish connectivity to {host}")

if __name__ == "__main__":
    main()
