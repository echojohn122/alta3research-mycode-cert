# alta3research-mycode-cert

Alta3 Network Automation Certification project.

This project will request an export of all the projects you owned on a gitlab instance.
It will wait for the export to be ready and download it.

## How to run

Run it with

```
python3 alta3research-pyna01.py
```

or

```
./alta3research-pyna01.py
```

## Usage
The script will promt you for the URL of your gitlab instance. (default is gitlab.com)
The script will then prompt you for your Personal Access Token (PAT).  This can be generated from your gitlab profile.
